import * as fs from "fs";
import fsp,{FileHandle} from "fs/promises";
import * as archiver from "archiver";

export class IOUtils {
    private static options: BufferEncoding = 'utf8';

    public static read(path: string): string[] {
        let s = fs.readFileSync(path, this.options);
        return s.split('\n').filter(line => line !== '');
    }

    public static createFile(path: string): void {
        fs.writeFileSync(path, '', this.options);
    }

    public static ensureOutputDirExists(dir: string) {
        return fsp.mkdir(dir, {recursive: true})
    }

    public static createFileAsync(path: string) {
        return fsp.writeFile(path, '', this.options);
    }

    public static appendToFile(filePath: string, content: string): void {
        fs.appendFileSync(filePath, content + "\n", this.options);
    }

    public static async openFileForAppending(filePath: string, cb: (handle: FileHandle) => Promise<unknown>): Promise<void> {
        const handle = await fsp.open(filePath, 'a')
        try {
            await cb(handle)
        } finally {
            await handle.close();
        }
    }

    public static zipSourceCode() {
        const output = fs.createWriteStream('output/source_code.zip');
        const archive = archiver.create('zip');
        archive.pipe(output);
        archive.on('error', function (err) {
            throw err;
        });
        archive.directory('src', false);
        archive.file('package.json', {name: 'package.json'});
        archive.file('tsconfig.json', {name: 'tsconfig.json'});
        archive.finalize();
    }
}
