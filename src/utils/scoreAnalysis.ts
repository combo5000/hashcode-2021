import * as fs from 'fs/promises'
import chalk from 'chalk'

export type ScoreSheet = Record<string, number>

export const loadCurrentScoresheet = async (): Promise<ScoreSheet> => {
    let content = await fs.readFile("scores.json");
    return JSON.parse(content.toString("utf-8")) as ScoreSheet;
}
export const saveScoresheet = async (sheet: ScoreSheet) => {
    await fs.writeFile('scores.json', JSON.stringify(sheet), {encoding: "utf-8"})
}

const calcPercentage = (current: number, previous: number) => +(((current / previous) - 1) * 100).toFixed(2);
export const logScores = async (previousScores: ScoreSheet, scores: ScoreSheet) => {
    const previousFiles = Object.keys(previousScores)
    const files = Object.keys(scores)
        .sort((a, b) => a.localeCompare(b))
    let previousTotal = 0, total = 0
    for (const file of files) {
        if (previousFiles.includes(file)) {
            const previousScore = previousScores[file]
            const score = scores[file]
            previousTotal += previousScore;
            total += score

            if (score > previousScore) {
                console.log(chalk.green(`Work: ${file} SCORE: ${score} score has improved ${calcPercentage(score, previousScore)}%`))
            } else if (score < previousScore) {
                console.log(chalk.red(`Work: ${file} SCORE: ${score} score is worse ${calcPercentage(score, previousScore)}%`))
            } else {
                console.log(chalk.yellow(`Work: ${file} SCORE: ${score} scores equal`))
            }
        }
    }

    if (total > previousTotal) {
        console.log(chalk.green(`Total Score: ${total} score has improved ${calcPercentage(total, previousTotal)}%`))
    } else if (total < previousTotal) {
        console.log(chalk.red(`Total Score: ${total} score is worse ${calcPercentage(total, previousTotal)}%`))
    } else {
        console.log(chalk.yellow(`Total Score: ${total} scores equal`))
    }

    await saveScoresheet(scores)
}
