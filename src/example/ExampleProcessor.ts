import {Processor} from "../Processor";
import {ScoreCalculator} from "./ScoreCalculator";

export class ExampleProcessor implements Processor{
    private input: string[];
    private output: string[];
    private totalPizzas: number;
    private teamOf2: number;
    private teamOf3: number;
    private teamOf4: number;

    constructor(firstLine: string) {
        let vars = firstLine.split(' ');
        this.totalPizzas = Number(vars[0]);
        this.teamOf2 = Number(vars[1]);
        this.teamOf3 = Number(vars[2]);
        this.teamOf4 = Number(vars[3]);
        this.input = [];
        this.output = [];
        console.log(this);
    }

    public async process(input: string[]): Promise<string[]> {
        this.input = input;
        let listToProcess = [...input];
        listToProcess.forEach((value, index) => {
            listToProcess[index] = index + " " + value;
        });
        this.output = this.estimateDelivery(listToProcess)
        return Promise.resolve(this.output);
    }

    private estimateDelivery(input: string[]): string[] {
        let pizzaToSend: string[] = [];
        let unProcessable = false;
        while (!unProcessable && input.length > 0) {
            if (this.teamOf4 > 0 && input.length > 4) {
                let order: string = '4 ';
                let toDeliver = input.splice(0, 4);
                toDeliver
                    .map(v => v.split(' ')[0])
                    .forEach(i => order += `${i} `);
                this.teamOf4--;
                pizzaToSend.push(order);
            } else if (this.teamOf3 > 0 && input.length > 3) {
                let order: string = '3 ';
                let toDeliver = input.splice(0, 3);
                toDeliver
                    .map(v => v.split(' ')[0])
                    .forEach(i => order += `${i} `);
                this.teamOf3--;
                pizzaToSend.push(order);
            } else {
                unProcessable = true;
            }
        }
        return pizzaToSend;
    }

    async getScore(): Promise<number> {
        return await ScoreCalculator.calcScore(this.input, this.output);
    }
}
