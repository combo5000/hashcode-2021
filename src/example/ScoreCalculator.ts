export class ScoreCalculator {
    public static async calcScore(original: string[], toValidate: string[]): Promise<number> {
        return new Promise(resolve => {
            let totalScore = 0;
            toValidate.forEach(value => {
                let strings: string[] = value.split(' ').filter(s => s !== '');
                strings.shift();
                const ingredients = strings.flatMap((index: string) => {
                    let i = original[Number(index)].split(' ');
                    i.shift();
                    return i;
                });
                const uniqueIngredients = [...new Set(ingredients)]
                totalScore += Math.pow(uniqueIngredients.length, 2);
            })
            resolve(totalScore);
        });
    }
}
