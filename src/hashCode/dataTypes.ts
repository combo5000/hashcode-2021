export type HashCodeInputData = {
    duration: number
    intersections: number
    streets: number
    cars: number
    bonus: number
    streetDescriptions: Record<string, StreetDescription>
    carPaths: CarPath[]
}
export type StreetDescription = {
    begin: Node
    end: Node
    name: StreetName
    length: number
    index: number
}
export type CarPath = {
    streetsNum: number
    streetNames: string[]
}


export type HashCodeOutputData = {
    intersections: Intersection[];
}


export type StreetName =string

export type Intersection = {
    index: Node
    streets: Record<StreetName, StreetDescription>
    business: Record<StreetName, number>
    schedule: Map<StreetName, number>
}
export type Node = number



export type EnhancedStreet = StreetDescription & { intersections: Intersection[] }