import {Processor} from "../Processor";
import {CarPath, HashCodeInputData, HashCodeOutputData, StreetDescription} from "./dataTypes";
import {scoreCalc} from "./scoreCalc";
import {runAlgo2} from "./Algorithms/algov2";
import {runAlgo3} from "./Algorithms/algov3";
import {runAlgo4} from "./Algorithms/algov4";

const deepCopy = <T>(data: T): T => JSON.parse(JSON.stringify(data))

function parseInput(input: string[]): HashCodeInputData {
    const firstLineValues = input[0].split(' ');

    const duration = Number(firstLineValues[0]);
    const intersections = Number(firstLineValues[1]);
    const streets: number = Number(firstLineValues[2]);
    const cars: number = Number(firstLineValues[3]);
    const bonus = Number(firstLineValues[4]);

    let streetDescriptions: Record<string, StreetDescription> = {};
    for (let i = 1; i <= streets; i++) {
        let streetInfo = input[i].split(' ');
        const customIndex = (i - 1);
        const street = {
            begin: Number(streetInfo[0]),
            end: Number(streetInfo[1]),
            name: streetInfo[2],
            length: Number(streetInfo[3]),
            index: customIndex
        } as StreetDescription;

        streetDescriptions[street.name] = street;
    }

    const carPaths: CarPath[] = []
    for (let j = (1 + streets); j <= cars+streets; j++) {
        const carInfo = input[j].split(' ');
        const car = {
            streetsNum: Number(carInfo[0]),
            streetNames: carInfo.slice(1)
        } as CarPath;
        carPaths.push(car);
    }

    return {
        duration: duration,
        intersections: intersections,
        streets: streets,
        cars: cars,
        bonus: bonus,
        streetDescriptions: streetDescriptions,
        carPaths: carPaths
    }
}

export default class HashCodeProcessor implements Processor {
    private input?: HashCodeInputData;
    private output?: HashCodeOutputData;

    private processData(data: HashCodeInputData): Promise<HashCodeOutputData> {
        // return runAlgo1(data);
        // return runAlgo2(data);
        // return runAlgo3(data);
        return runAlgo4(data);
    }

    public async process(input: string[]): Promise<HashCodeOutputData> {
        const data = parseInput(input)
        this.input = data;
        this.output = await this.processData(data)
        return Promise.resolve(this.output);
    }

    public getScore(): Promise<number> {
        if (!this.input || !this.output) throw new Error("input or output has not been defined")

        return Promise.resolve(scoreCalc(this.input, this.output));
    }
}
