import {CarPath, EnhancedStreet, HashCodeInputData, HashCodeOutputData, Intersection, StreetName} from "../dataTypes";

const buildIntersections = (data: HashCodeInputData): [Intersection[], Record<StreetName, EnhancedStreet>] => {
    const {intersections, streetDescriptions} = data
    const enhancedStreets: Record<StreetName, EnhancedStreet> = {}

    const intersectionList: Intersection[] = [];
    for (let i = 0; i < intersections; i++) {
        intersectionList[i] = {
            index: i,
            streets: {},
            business: {},
            schedule: new Map()
        }
    }
    for (const description of Object.values(streetDescriptions)) {
        const enhanced = description as EnhancedStreet

        const begintersection = intersectionList[description.begin]
        // begintersection.streets[description.name] = description
        // begintersection.business[description.name] = 0

        const endersection = intersectionList[description.end]
        endersection.streets[description.name] = description
        endersection.business[description.name] = 0

        enhanced.intersections = [begintersection, endersection]
        enhancedStreets[enhanced.name] = enhanced
    }
    return [intersectionList, enhancedStreets]
}
const calculateBusiness = (interections: Intersection[], carPaths: CarPath[], streets: Record<StreetName, EnhancedStreet>) => {
    for (const carPath of carPaths) {
        for (let i = 0; i < carPath.streetNames.length; i++) {
            const streetName = carPath.streetNames[i]
            const street = streets[streetName as StreetName]

            street.intersections[1].business[street.name]++
            // if(i!==0){
            //     street.intersections[0].business[street.name]++
            // }
        }
    }
}
const calculateSchedules = (interections: Intersection[]) => {
    for (const intersection of interections) {
        //const businesses = Object.values(intersection.business).sort((a,b)=>a-b)
        const streets = Object.keys(intersection.streets);
        for (const street of streets) {
            const incoming = intersection.streets[street];
            if (intersection.business[street] !== 0) {
                let duration = Math.round(intersection.business[street] / 4)

                intersection.schedule.set(street, Math.max(1, duration))
            }
        }
    }
}

export const runAlgo3 = (data: HashCodeInputData): Promise<HashCodeOutputData> => {
    const [intersections, enhancedStreets] = buildIntersections(data)
    calculateBusiness(intersections, data.carPaths, enhancedStreets)
    calculateSchedules(intersections)
    let filtered = intersections.filter(value => value.schedule.size > 0)
    return Promise.resolve({intersections: filtered})

}