#!/usr/bin/env node
import {IOUtils} from "./utils/IOUtils";
import {ExampleProcessor} from "./example/ExampleProcessor";
import {loadCurrentScoresheet, logScores, ScoreSheet} from "./utils/scoreAnalysis";
import HashCodeProcessor from "./hashCode/HashCodeProcessor";
import {HashCodeOutputData} from "./hashCode/dataTypes";

const figlet = require('figlet');

console.clear();

console.log(figlet.textSync('Google Hashcode 2021', {horizontalLayout: 'full'}));

const inputDir = 'input/'
const outputDir = 'output/'

// const subdir = 'example/'

const subdir = 'hashcode/'

async function writeOutput(outputFile: string, output: HashCodeOutputData) {
    IOUtils.appendToFile(outputFile, String(output.intersections.length));
    await IOUtils.openFileForAppending(outputFile, async handle => {
        for (const intersection of output.intersections) {
            await handle.write(String(intersection.index) + "\n");
            await handle.write(String(intersection.schedule.size) + "\n");
            for (const key of intersection.schedule.keys()) {
                let duration = intersection.schedule.get(key);
                await handle.write(`${key} ${duration}\n`);
            }
        }
    });
}

async function startHashcodeWork(fileName: string, scores: ScoreSheet) {
    let workLabel = "work-" + fileName;
    console.time(workLabel)

    let content = IOUtils.read(inputDir + subdir + fileName);
    let outputFile = outputDir + subdir + fileName.replace('.in', '.out');
    let outputFilePromise = IOUtils.createFileAsync(outputFile);

    const p = new HashCodeProcessor();

    const output: HashCodeOutputData = await p.process(content)
    await outputFilePromise;

    await writeOutput(outputFile, output);

    console.timeEnd(workLabel)

    // scores[fileName] = await p.getScore()
}

async function startExampleWork(fileName: string, scores: ScoreSheet) {
    let workLabel = "work-" + fileName;
    console.time(workLabel)

    let content = IOUtils.read(inputDir + subdir + fileName);
    let outputFile = outputDir + subdir + fileName.replace('.txt', '.out');
    let outputFilePromise = IOUtils.createFileAsync(outputFile);

    const p = new ExampleProcessor(content[0]);
    content.shift();

    const orders = await p.process(content)
    await outputFilePromise;


    IOUtils.appendToFile(outputFile, String(orders.length));
    await IOUtils.openFileForAppending(outputFile, async handle => {
        for (const order of orders) {
            await handle.write(order + '\n');
        }
    })
    console.timeEnd(workLabel)

    scores[fileName] = await p.getScore()
}

async function startHashcodeWorkload() {
    let previousScores = await loadCurrentScoresheet();
    let scores: ScoreSheet = {};

    await IOUtils.ensureOutputDirExists(outputDir + subdir)

    console.time('workload')
    await startHashcodeWork('a.txt', scores)
    await startHashcodeWork('b.txt', scores)
    await startHashcodeWork('c.txt', scores)
    await startHashcodeWork('d.txt', scores)
    await startHashcodeWork('e.txt', scores)
    await startHashcodeWork('f.txt', scores)

    console.timeEnd('workload')

    // await logScores(previousScores, scores)
}

async function startExampleWorkload() {
    let previousScores = await loadCurrentScoresheet();
    let scores: ScoreSheet = {};

    await IOUtils.ensureOutputDirExists(outputDir + subdir)

    console.time('workload')
    await startExampleWork('a_example.in', scores)
    await startExampleWork('b_little_bit_of_everything.in', scores)
    await startExampleWork('c_many_ingredients.in', scores)
    await startExampleWork('d_many_pizzas.in', scores)
    await startExampleWork('e_many_teams.in', scores)

    console.timeEnd('workload')

    await logScores(previousScores, scores)
}

// startExampleWorkload();
startHashcodeWorkload();

// Uncomment when needed
